def search_for_words(grid, search_terms):

    results = []

    for term in search_terms:
        for row in range(len(grid)):
            for col in range(len(grid[row])):
                # Check horizontal
                if col + len(term) <= len(grid[row]):
                    if ''.join(grid[row][col:col+len(term)]) == term:
                        results.append((term, (row, col), (row, col+len(term)-1)))
                    if ''.join(grid[row][col:col+len(term)]) == term[::-1]:
                        results.append((term, (row, col+len(term)-1), (row, col)))

                # Check vertical
                if row + len(term) <= len(grid):
                    if all(grid[row+i][col] == term[i] for i in range(len(term))):
                        results.append((term, (row, col), (row+len(term)-1, col)))
                    if all(grid[row+i][col] == term[::-1][i] for i in range(len(term))):
                        results.append((term, (row+len(term)-1, col), (row, col)))

                # Check diagonal (top-left to bottom-right)
                if row + len(term) <= len(grid) and col + len(term) <= len(grid[row]):
                    if all(grid[row+i][col+i] == term[i] for i in range(len(term))):
                        results.append((term, (row, col), (row+len(term)-1, col+len(term)-1)))
                    if all(grid[row+i][col+i] == term[::-1][i] for i in range(len(term))):
                        results.append((term, (row+len(term)-1, col+len(term)-1), (row, col)))

                # Check diagonal (bottom-left to top-right)
                if row - len(term) >= 0 and col + len(term) <= len(grid[row]):
                    if all(grid[row-i][col+i] == term[i] for i in range(len(term))):
                        results.append((term, (row, col), (row-len(term)+1, col+len(term)-1)))
                    if all(grid[row-i][col+i] == term[::-1][i] for i in range(len(term))):
                        results.append((term, (row-len(term)+1, col+len(term)-1), (row, col)))

    return results

def main():
    # Load input file
    with open('input.txt', 'r') as file:
        lines = file.readlines()

    # Parse input file
    size = lines[0].strip().split('x')
    rows = int(size[0])
    cols = int(size[1])
    grid = [line.strip().split() for line in lines[1:rows+1]]
    search_terms = [line.strip() for line in lines[rows+1:]]

    # Search for words in grid
    results = search_for_words(grid, search_terms)

    # Print output
    for term, start, end in results:
        print(f"{term} {start[0]}:{start[1]} {end[0]}:{end[1]}")

if __name__ == "__main__":
    main()